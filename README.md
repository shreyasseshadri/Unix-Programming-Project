
# Velocity raptor
## About the game:
<br>
This is a game developed purely in bash without using any external libraries.

## Rule(Aim) of the game:
<span>The aim of the game is simple,"Avoid obstacles!"</span>

* In this game the more the player avoid obstacles,the more points he gets
* As the player's points increases the speed of the obstacles increase,making it more difficult than the previous level
* Many models are available,and the player can choose anyone of them according to their need.
* Models have different attributes attatched to them like width,height,speed of ascent and descent,etc...Hence,while choosing the model the player  can make appropriate choices.

## Basic Contols:

* `model# `:  The model number shown on the screen can be pressed to choose a particular model.
* `Enter` : Press Enter to start the game,or "q" to quit.</li>
* `q`: Quits the game.
## How to run the game

* On the top corner you will find an option to clone this repository.Click the option and copy the https link.
* Once in your terminal type in 
```git clone <copied_link> ```.<br>*This assumes that you have installed git*.<br>If not [download from this link](https://git-scm.com/download/linux).
* Once you have cloned our repository you will find a folder named `Velocity_Raptor`.Type in `cd Velocity_Raptor` in your terminal.
* Then run the game using `./run.sh`
## Happy gaming!